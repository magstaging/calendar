This module adds a calendar that can be setup to show unavailable dates in the frontend magento template

    clone the repository

    create folder app/code/Mbs/DateCalendar when located at the root of the Magento site

    copy the content of this repository within the folder

    install the module php bin/magento setup:upgrade

    load any page of the site and verify the page shows a calendar:w

