define([
    'jquery',
    'mage/calendar'
], function ($) {
    'use strict';

    var unavailableDates = ["03-11-2018","09-11-2018","11-11-2018"];
    function unavailable(date) {
        var string = $.datepicker.formatDate('dd-mm-yy',date);
        return [unavailableDates.indexOf(string) == -1];
    }

    return function (config, element) {
        $(element).calendar({beforeShowDay: unavailable});
    }
});